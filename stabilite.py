import feedparser
import requests
import config
import time
import json


def json_dump(filename, content):
    with open(filename, "w") as f:
        json.dump(content, f)


def json_load(filename):
    with open(filename, "r") as f:
        return json.load(f)


# Load data from json and fetch feed
data = json_load("data.json")
feed = feedparser.parse('https://yls8.mtheall.com/ninupdates/feed.php')

# Go through all feed entries
for entry in feed["entries"]:
    # Hackily find version and console name from RSS entry name
    # as this info is not provided in any other way
    version = entry["title"].split(" ")[-1]
    console = entry["title"].replace(version, " ").strip()
    link = entry["link"]

    # Only post if there is actually an update
    publish_time = time.mktime(entry["published_parsed"])
    if publish_time <= data[console]["lastupdate"]:
        continue

    data[console] = {"version": version,
                     "lastupdate": publish_time}

    # Derive text to be sent to webhooks
    hook_text = f"🚨 **System update detected for {console}: {version}**\n"\
                f"More information at <{link}>\n"\
                "Do not update until further notice."
    print(hook_text)

    # Send notification to the webhooks
    for webhook in config.notify[console]:
        requests.post(webhook, data={"content": hook_text})

# Save new data to the json
json_dump("data.json", data)
