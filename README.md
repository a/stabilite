# stabilité

Script to fetch Nintendo console update news from ylws8's website, and to push them through a webhook.

If you want the version that I host to post through your webhooks too, hmu (email is on gitlab profile).

Shoutouts to ihaveahax for passing on his version to me, which is what this is roughly based on.

---

## Screenshot

![](https://elixi.re/i/kcn00d77.png)

---

## How to use

- Install python3.6+
- Install packages in requirements.txt (`pip3 install -Ur requirements.txt`)
- Copy files that end with `.template` to non-`.template` versions.
- Configure `config.py`
- Run the script, and run it periodically with cron or systemd timers or whatever. `systemd-timer` has an example config for systemd timers.

